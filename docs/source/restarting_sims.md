# Continuing Simulations

We can dump the 'internals' of a simulation (the `status` of all the nodes, the `event_queue`, and some of the `sim_objects`), and use those to recreate the state of the simulation at the time that it was dumped. This allows us to 'continue' simulations from the endpoints of other simulations, perhaps with different parameters. This is done by pickling these objects to a file on disk with the suffix `.internals.pkl`.

![Restarting Sims](img/restart.png)

```{admonition} Sharp Corner #1

Because this restart is relative 'dumb', it cannot detect events that have been queued that aren't 'meant' to be triggered in following simulations. For example, scheduled lockdowns (non-conditional lockdowns) are queued as part of the `initial_delays`, and are 'carried through' to future simulations. 
```

```{admonition} Sharp Corner #2

The `tmin` (starting time) of the subsequent simulation is set by the maximum time reached in the initial simulation. Due to the interaction of pseudorandom numbers and truncation of events past `tmax`, this can mean that events in subsequent simulations can occur 'before' the `tmax` of the initial simulation.
```

```{caution}

Some objects in Python cannot be pickled, and unpickling objects is sometimes hazardous.
For example, we cannot pickle the cached gamma function (since it is a local function), so we have to regenerate this for subsequent simulations. Currently, this does not pose a problem, but as new features are added, we may run into issues where non-deterministic objects cannot be pickled (for example networkx Graph objects).
```

