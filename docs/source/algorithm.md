# Simulating discrete jumps in time

We note that for all the processes involved in the model, they correspond to discrete events where the system (in this case New Zealand) changes from one state to another (for example an individual becoming infected changes the state of the system by decreasing the number of susceptible individuals by one, and increasing the number of infectious individuals by one). These changes by discrete events are so called 'jump processes', and the frequency and nature of these jump events are controlled by user-defined reactions, with a corresponding a rate or propensity of occurring.

There have been various methods successfully developed for the simulation of jump processes, including Markov Chain Monte Carlo based methods and others, but we make use of a well-known algorithm and its variants called the Gillespie Method. The Gillespie Method (especially its direct variant) is a well-established method for simulating stochastic jump process models. Originally developed for chemical reaction networks, it now sees wide use, including in mathematical biology.

We outline below the original method Gillespie Direct Method which forms the basis of our more nuanced rejection-based method and then introduce our main simulation method (dubbed the "Gillespie Max Method").

## Gillespie's Direct Method

Gillespie's direct method allows us to simulate Markovian processes, which have an exponentially-distributed inter-event time. The generated histories are 'exact' in the sense that the distributions of the observed inter-event times are exact with respect to the specified distributions. In our model, the majority of our jump processes have exponentially-distributed inter-event times (such as infection rate, the rates of hospitalisation, recovery, transition into critical care and more).

```{pcode}
---
linenos: True
---

\begin{algorithm}
\caption{Gillespie's Direct Method}
\begin{algorithmic}
\INPUT a bipartite network $G=(N_I, N_G, E)$ with individual nodes $N_I$, group node $N_G$ and edges $E$, reactions $R$ indexed by index set $\mathcal{I}$ and their propensities $P$, the initial state of the nodes $\mathcal{X}_0$ at the initial time $t_0$, and maximum simulation time $t_{\text{max}}$.
\OUTPUT a history of events $H$
\STATE Compute the total reaction propensity $P_0 = \sum_{i\in\mathcal{I}} {p}_i$ 
\WHILE {$t < t_{\text{max}}$ \AND $P_0 > 0$}
    \STATE Draw time to next potential event $\Delta t \sim \text{Exp}(P_0)$
    \STATE $t \gets t + \Delta t$
    \STATE Draw event $i \in \mathcal{I}$ weighted by $\frac{{p}_i}{P_0}$
    \STATE Execute the event $r_i$ by updating the state $\mathcal{X}$
    \STATE Record the state to history $H$
    \STATE Recompute any  propensities $p_j$ of reactions affected by the event
    \STATE Compute the total propensity $P_0 = \sum_{i \in \mathcal{I}} {p}_i$
\ENDWHILE
\end{algorithmic}
\end{algorithm}
```

## Gillespie Max

However, to deal with semi-Markovian and non-Markovian reactions, we use a rejection-based method that we dub "Gillespie Max". This is essentially a subset of Lok's "first-family" method. Using rejection-based sampling also allows for efficient simulation of demographic- and context-dependent transition rates (such as having different spreading rates in different contexts and different hospitalisation rates for more at-risk individuals). We note for clarity that semi-Markovian processes can be defined as processes whose transition rate does not depend on the transition history but only on the time spent in the current state (e.g. the waning of an individual's protection from vaccination and associated infection/symptom/etc. probabilities) and that non-Markovian processes are processes whose transition rate does depend on the transition history.

```{pcode}
---
linenos: True
---

\begin{algorithm}
\caption{Gillespie Max}
\begin{algorithmic}
\INPUT a bipartite network $G=(N_I, N_G, E)$ with individual nodes $N_I$, group node $N_G$ and edges $E$, reactions $R$ indexed by index set $\mathcal{I}$ and their propensities $P$, the initial state of the nodes $\mathcal{X}_0$ and event queue $Q$ at the initial time $t_0$, and maximum simulation time $t_{\text{max}}$.
\OUTPUT a history of events $H$
\STATE Compute maximum propensities $\hat{p}_i = \max_{\tau \in [t, t+h)} p_i(\tau)$ for each reaction $r_i \in R$.
\STATE Compute the total reaction propensity $P_0 = \sum_{i\in\mathcal{I}} \hat{p}_i$ 
\WHILE {$t < t_{\text{max}}$ \AND ($Q$ not empty \OR $P_0 > 0$)}
    \IF {$P_0 > 0$}
        \STATE Draw time to next potential event $\Delta t \sim \text{Exp}(P_0)$
    \ELSE
        \STATE $\Delta t \gets 0$
    \ENDIF
    \IF {$Q$ not empty \AND \texttt{next}$(Q).t < (t + \tau)$}
        \STATE Set next event $v \gets$ \texttt{next}$(Q)$
        \STATE $t \gets v.t$
    \ELSE
        \STATE $t \gets t + \Delta t$
        \STATE Draw event $i \in \mathcal{I}$ weighted by $\frac{\hat{p}_i}{P_0}$
        \STATE Draw $a \sim U(0, 1)$
        \IF {$a < {p_i}/{\hat{p}_i}$}
            \STATE Set next event $v \gets r_i$
        \ELSE
            \STATE Set next event $v \gets \emptyset$ (phantom event)
        \ENDIF
    \ENDIF

    \IF {$v \neq \emptyset$}
        \STATE Execute the event $v$ by updating the state $\mathcal{X}$
        \STATE Record the state to history $H$
        \STATE Schedule any events to occur in the future by inserting into $Q$
        \STATE Recompute any (maximum) propensities $p_j, \hat{p}_j$ of reactions affected by the event
        \STATE Compute the total propensity $P_0 = \sum_{i \in \mathcal{I}} \hat{p}_i$
    \ENDIF
\ENDWHILE
\end{algorithmic}
\end{algorithm}
```

<hr />
