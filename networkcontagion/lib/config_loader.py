""" Parser for a YAML file with a custom arithmetic tag """

import os
import yaml
import re
from pathlib import Path
from collections import OrderedDict
import numpy as np
from typing import Any, IO
import csv

from . import randomfloat

# build custom Config Loader
class ConfigLoader(yaml.SafeLoader):
    """Custom YAML Loader for networkcontagion configurations.
    
    Implements custom tags:

    * ```!+``` and ```!math``` for simple (one-operator) arithmetic expressions
    * ```!include``` for nested external YAML imports
    * ```!paramtable``` for multi-key lookup tables
    * ```!()``` shorthand for tuples
    * ```!sumdict``` for combining dictionaries/mappings 

    as well as defaulting all mappings to an OrderedDict type
    """

    def __init__(self, stream: IO) -> None:
        """Initialise an empty contagion record
        
        Args:
            stream (IO): data stream to load YAML from (in this case .yml)
        """
        try:
            # _root is the directory containing the root stream file
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir

        super().__init__(stream)

class ConfigDumper(yaml.SafeDumper):
    """Custom YAML Dumper for dumping networkcontagion configurations.
    """
    pass

class FrozenConfigDumper(ConfigDumper):
    """Custom YAML Dumper for dumping frozen networkcontagion configurations.
    """
    pass

__math_re = re.compile(r"\s*([\d\.]+)\s*([\+\-\*\/])\s*([\d\.]+)\s*")

class MathParseError(Exception):
    """Raised for non-valid arithmetic expressions"""
    pass

# For simple math handling
def math_compute(loader: ConfigLoader, node: yaml.Node) -> float:
    """ Simple YAML tag arithmetic parser function for one-operator expressions
    
    Deals with four elementary arithmetic operations

    Raises: MathParseError
        if the arithmetic expression is not valid
    """
    math_args = loader.construct_scalar(node)
    comp_args = __math_re.match(math_args)
    if comp_args is None:
        raise MathParseError(f"{math_args} not a valid expression")
    arg1, operator, arg2 = comp_args.groups()
    if operator == "+":
        return float(arg1) + float(arg2)
    elif operator == "-":
        return float(arg1) - float(arg2)
    elif operator == "*":
        return float(arg1) * float(arg2)
    elif operator == "/":
        return float(arg1) / float(arg2)

# For Ordered input dicts
def construct_mapping(loader: ConfigLoader, node: yaml.Node) -> OrderedDict:
    """ Constructs a flattened mapping of the YAML file for data parsing
    """
    loader.flatten_mapping(node)
    return OrderedDict(loader.construct_pairs(node))

# For external file includes
def construct_include(loader: ConfigLoader, node: yaml.Node) -> Any:
    """ External file includer

    Yanked from https://gist.github.com/joshbode/569627ced3076931b02f
    """
    filename = os.path.abspath(os.path.join(loader._root, loader.construct_scalar(node)))
    extension = os.path.splitext(filename)[1].lstrip('.')

    with open(filename, 'r') as f:
        if extension in ('yaml', 'yml'):
            return yaml.load(f, ConfigLoader)
        if extension in ('csv'):
            true_rel_path = os.path.relpath(filename)
            if true_rel_path[:14] == '../../../../..':
                true_rel_path = true_rel_path[14:]
            return construct_csv(f, name=true_rel_path)
        else:
            return ''.join(f.readlines())

class ParameterTable(dict):
    """A lookup table that takes an attribute dictionary as a key
    
    Used for looking up node attributes."""
    _csv_types = {
        'TA': int,
        'age': int,
        'ethnicity': str,
    }

    def __init__(self, *args, keytype=None, extname=None, **kwargs):
        """Constructor for a ParameterTable

        Parameters:
            keytype (list[str]): names of the attributes to lookup. Defaults to empty list.
                Additionally defines the types to cast the attributes to.
            extname (str, optional): name of the external file used to construct the table.
                Defaults to None, and is ignored if so.
            args, kwargs: passed to the dict constructor
        """
        super().__init__(*args, **kwargs)
        if keytype is None:
            keytype = []
        self._keytype = keytype
        self.casts = [self._csv_types.get(key, lambda x: x) for key in self._keytype]
        self.extname = extname
        self._max_value = None

    @property
    def max_value(self):
        """Maximum value in the table"""
        if self._max_value is None:
            self._max_value = max(self.values())
        return self._max_value

    @property
    def keytype(self):
        """List of attributes to lookup"""
        return self._keytype

    def lookup(self, obj):
        """Lookup function using a dictionary's values as a key
        
        Args:
            obj (dict): dictionary that contains the keytypes in its keys.
        """
        if len(self._keytype) == 1:
            return self[obj[self._keytype[0]]]
        key = tuple(cast(obj[k]) for k, cast in zip(self._keytype, self.casts))
        return self[key]

    def __eq__(self, other):
        if not isinstance(other, ParameterTable):
            return False
        eqval = super().__eq__(other)
        return eqval and self.keytype==other.keytype

def construct_csv(f: IO, name: str=None) -> dict:
    """Constructs a ParameterTable from a csv file
    
    Args:
        f: IO stream that contains a csv-formatted data
        name (optional): Name of the file that the IO stream is opened from, if any.
    """
    reader = csv.DictReader(f)
    _type = ParameterTable._csv_types
    fields = reader.fieldnames[:-1]
    prob = reader.fieldnames[-1]
    if name is None:
        name = f.name
    if len(fields) == 1:
        field = fields[0]
        return ParameterTable({_type[field](x[field]): float(x[prob]) for x in reader}, keytype=fields, extname=name)
    return ParameterTable({tuple(_type[field](x[field]) for field in fields): float(x[prob]) for x in reader}, keytype=fields, extname=name)

def construct_parameter_table(loader, node):
    """Constructs a ParameterTable from a YAML node
    
    Args:
        loader: YAML loader to use
        node: YAML node to construct table from
    """
    loader.flatten_mapping(node)
    node_dict = dict(loader.construct_pairs(node))
    keytype = node_dict.pop('keytype')
    return ParameterTable(node_dict, keytype=keytype)

def construct_tuple(loader, node):
    """Constructs a tuple from a YAML node
    
    Args:
        loader: YAML loader to use
        node: YAML node to construct tuple from
    """
    return tuple(loader.construct_sequence(node))

def construct_added_dict(loader, node):
    """Constructs a tuple from a YAML node
    
    Args:
        loader: YAML loader to use
        node: YAML node to construct tuple from
    """
    list_of_dicts = loader.construct_sequence(node)
    sum_dict = OrderedDict()
    for elem_dict in list_of_dicts:
        sum_dict.update(elem_dict)
    return sum_dict

def represent_ordered_dict(loader, node):
    """Represents YAML node as an ordered dict
    """
    return loader.represent_mapping('tag:yaml.org,2002:map', node.items())

def represent_numpy_float64(loader, node):
    """Represents YAML node as a float
    """
    return loader.represent_float(node)

def represent_numpy_str(loader, node):
    """Represents YAML node as a string
    """
    return loader.represent_str(node)

def represent_parameter_mapping(loader, node):
    """Represents YAML nodes as appropriate for included csvs and paramtables
    """
    if node.extname is not None:
        return loader.represent_scalar('!include', node.extname)
    node_dict = dict(node)
    node_dict['keytype'] = node.keytype
    return loader.represent_mapping('!paramtable', node_dict)

def represent_tuple(loader, node):
    """Represents YAML node as a tuple
    """
    return loader.represent_sequence('!()', node)

yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, construct_mapping, ConfigLoader)
yaml.add_constructor(u'!math', math_compute, ConfigLoader)
yaml.add_constructor(u'!+', math_compute, ConfigLoader)
yaml.add_constructor(u'!include', construct_include, ConfigLoader)
yaml.add_constructor(u'!paramtable', construct_parameter_table, ConfigLoader)
yaml.add_constructor(u'!()', construct_tuple, ConfigLoader)
yaml.add_constructor(randomfloat.random_tag, randomfloat.construct_random_float, ConfigLoader)
yaml.add_constructor(u'!sumdict', construct_added_dict, ConfigLoader)

yaml.add_representer(OrderedDict, represent_ordered_dict, ConfigDumper)
yaml.add_representer(np.float64, represent_numpy_float64, ConfigDumper)
yaml.add_representer(np.str_, represent_numpy_str, ConfigDumper)
yaml.add_representer(ParameterTable, represent_parameter_mapping, ConfigDumper)
yaml.add_representer(tuple, represent_tuple, ConfigDumper)
yaml.add_representer(randomfloat.RandomFloat, randomfloat.represent_random_float, ConfigDumper)

yaml.add_representer(randomfloat.RandomFloat, randomfloat.represent_frozen_random_float, 
                     FrozenConfigDumper)

def load(filename: str) -> Any:
    """Loads a YAML file, using additional custom tags.

    Args:
        filename: Path to YAML file to load

    Returns:
        A dictionary corresponding to the file loaded
    """
    # File exists check
    assert Path(filename).is_file(), f"Configuration file {filename} does not exist"

    with open(filename, 'r') as filepntr:
        data = yaml.load(filepntr, ConfigLoader)

    return data

def dump(data, filename, frozen=False):
    """Dumps a networkcontagion configuration to YAML file
    
    Args:
        data: Configuration dictionary
        filename: Path to file to dump to. Creates if does not exist, overwrites if it does.
        frozen: Whether or not to freeze values (e.g. of random numbers)
    """
    if not frozen:
        dumper = ConfigDumper
    else:
        dumper = FrozenConfigDumper

    with open(filename, 'w') as filepntr:
        yaml.dump(data, filepntr, Dumper=dumper, sort_keys=False)
