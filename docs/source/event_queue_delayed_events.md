# The Event Queue and Delayed Events
Delayed events are events that are scheduled at some determined point in the future. These differ from the other, stochastic transition events which have their random event time possibly drawn during the 'time-stepping' while-loop within [Gillespie Max](algorithm). The delayed events, such as exiting quarantine after a set time or contact tracing an individual after a set time, are instead added to an event queue (described below) and will be 'chosen' as the next occurring event if the time drawn to the next random event is drawn to occur after it. As a more concrete example, if the current simulation time is 2 days and individual X has recently entered a 5 day quarantine, then an event corresponding to his release will be added to the event queue. Now, if the next potential event time (let's say corresponding to individual Y infecting individual Z) is drawn to be at 7.5 days, the algorithm would see that the quarantine escape event in the event queue has an earlier occurrence than the randomly drawn infection event. The next chosen event would then be the quarantine escape instead and the system time would be updated to 7 days.

The `event_queue` object is a [`SortedList`](https://grantjenks.com/docs/sortedcontainers/sortedlist.html), which contains tuples of a particular structure:

```
(time of delayed effect, type of delayed effect, ... [additional event information] ...)
```

The type of delayed effect is one of the multiple enums defined in {any}`cobin.Event`.

The structure of additional event information is different per event type:

:::{table} Additional Event Information
:align: center

| Event Type | Additional Information |      |      |      |      |      |
| :--------- | :-- | :--- | :--- | :--- | :--- | :--- |
| `spontaneous` | name of node transitioning | status that the node is transitioning to |
| `infect` | name of infector node |
| `seek_test` | name of node seeking test | (optional) node that is causing test-seeking | (optional) context that is causing test-seeking |
| `get_test_result` | name of node that is getting test result | test result | (optional) node that is causing test-seeking | (optional) context that is causing test-seeking |
| `become_detected` | name of node that is being detected | new status of that node | (optional) node that is causing detection | (optional) context that is causing detection |
| `trace` | name of node to trace | number of attempts so far | re-attempt rate parameter | probability of attempt failure | (optional) node that is causing tracing | (optional) context that is causing tracing |
| `lockdown` | name of lockdown event |
| `escape` | name of node leaving isolation | strength of isolation (deprecated) |
| `reseed` | parameters to pass to reseeding function |
| `removal` | name of node being removed (recovering or dying) | state that node is transitioning to |
| `recovery_waning` | name of node becoming susceptible |

:::
