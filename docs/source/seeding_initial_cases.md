# Seeding initial cases
We have many (mutually exclusive) ways of defining subpopulations to draw the initial seeds from.
These can be split into two classes:
1. explicitly specified subpopulations
2. implicitly specified subpopulations

Explicitly specified subpopulations include
1. Specific individuals to seed: `seed_individual`
    - The `seed_individual` can either be:
        - a single individual (in which case it needs to be a string), or 
        - a list of individuals (list of strings)
2. Specific contexts/groups to randomly draw individuals from: `group_lists`
    - historical alias: `dwellings` 
    - specify names of files that contain row-delimited names of groups
    - optional `random_from_groups` (default `False`): 
        - if `True`, chooses one group each file to seed
        - if `False` seeds all groups in each file.
    - optional `whole_group` (default `False`): 
        - if `True`, seeds all individuals in groups of type specified in `whole_group_type`
        - if `False`, seeds one random individual in each group to seed
    - optional `whole_expose_type` (default `[DW]`): a list of group types for which to seed all individuals in the group (if `whole_groups` is `True`)

Implicitly specified subpopulations include
1. Seeding all individuals: `seed_all`
2. Random seeding: `random`
    - Subpopulations can be specified by `TA` or `SA2`, which limits seeding to groups that belong to the specified TA or SA2
    - `TA` and `SA2` are mutually exclusive, with `TA` having higher priority.
    - the (minimum) number of random seeds is determined by the option `num_seeds`
    - seeding is done by first drawing groups, and then individuals from those groups
    - optional `whole_group` (default `False`): 
        - if `True`, seeds all individuals in groups of type specified in `whole_group_type`
        - if `False`, seeds one random individual in each group to seed
    - optional `whole_expose_type` (default `[DW]`): a list of group types for which to seed all individuals in the group (if `whole_groups` is `True`)
    - in this way, we may get more individuals seeded than requested.


Post-initialisation, all seeded individuals have a status `EU`, non-seeded individuals have a status `SU` and all groups are open (status `00`).