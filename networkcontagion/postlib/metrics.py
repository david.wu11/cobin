""" Metrics library for post-processing """

from functools import partial
from . import behaviour, tracing, detection, plot

# Comment/uncomment desired timeseries to be returned
# Dictionary of timeseries metrics to return. Key:value == Timseries name:function
timeseries = {
    # Cases
    'New Infections': behaviour.byday_new_infections,
    'New Reseeds': behaviour.byday_new_reseeds,
    'New Confirmed Cases': behaviour.byday_new_conf,
    'New Hospitalisations': behaviour.byday_new_hosp,
    'New Critical': behaviour.byday_new_crit,
    'New Deaths': behaviour.byday_new_dead,
    'Newly Contact Traced': behaviour.byday_new_contact_trace,
    'Active Infections': behaviour.byday_active_infections,
    'Active Confirmed Cases': behaviour.byday_confirmed_cases,
    'Active in Isolation': behaviour.byday_isolated_cases,
    'Active Hospitalisations': behaviour.byday_current_hospitalised, 
    'Active Critical': behaviour.byday_current_icu,
    'Cumulative Infections': behaviour.byday_cumulative_infections,
    'Cumulative Reseeds': behaviour.byday_cumulative_reseeds,
    'Cumulative Confirmed Cases': behaviour.byday_cumul_conf_cases,
    'Cumulative Hospitalisations': behaviour.byday_cumul_hosp,
    'Cumulative Critical': behaviour.byday_cumul_crit,
    'Cumulative Deaths': behaviour.byday_cumul_dead
}

# Comment/uncomment desired groups to group timeseries by
timeseries.update(behaviour.byday_infection_groups)

# Comment/uncomment desired numerical metrics to return
# Dictionary of numerical/column metrics to return. Key:value == Metric name:function
columns = {
    #'Time at End of Simulation': behaviour.time_end,
    # -- Detection Metrics ------------------
    'Time to Detection': detection.time_of,
    'Cumulative Infections at Detection': detection.cumulative_cases,
    'Active Infections at Detection': detection.active_cases,
    # -- Behaviour Metrics ------------------
    'Total Infections': behaviour.total_cases,
    'Total Deaths': behaviour.total_deaths,
    'Total Hospitalisations': behaviour.total_hospitalisations,
    'Total Critical Hospitalisations': behaviour.total_icu,
    # -- Tracing Metrics ------------------
    'Peak Number of Individuals Isolated in a Day': tracing.peak_trace,
    'Day of Peak Individuals Isolated in a Day': tracing.time_peak_trace,
    'Total Number of Individuals Isolated': tracing.total_trace,
    'Mean Time to Trace Contacts': tracing.mean_time_to_trace
}

# Comment/uncomment desired plots to save
# Dictionary of plots to return. Key:value == Plot name:function
plots = {
    'plot_with_quantile': plot.plot_with_quantile
}
