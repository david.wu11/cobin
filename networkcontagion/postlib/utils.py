""" General utilities and variables """
import re
import warnings
import subprocess
import numpy as np
import pandas as pd
import networkx as nx
from itertools import product
from networkcontagion.lib import cobin
import pickle

def do_nothing(*args, **kwargs):
    """ The ultimate do nothing function """
    pass

all_states = [k for k,v in cobin._all_states.items() if '[GROUP]' not in v]

progression_transitions = {
    'S': 'E',
    'E': ('A', 'B', 'P'),
    'A': 'R',
    'B': 'M',
    'M': ('R', 'D'),
    'P': 'I',
    'I': ('R', 'D', 'H'),
    'H': ('R', 'D', 'C'),
    'C': ('R', 'D')
}

def state_prod(s, c):
    """ For usage within utils.py in joining sets of states """
    return set(map(''.join, product(s, c)))

# Base sets of states for usage in metrics
risky_states = ('E', 'A', 'B', 'P', 'M', 'I',)
infected_states = ('E', 'A', 'B', 'P', 'M', 'I', 'H', 'C',)
testable_states = ('A', 'B', 'P', 'M', 'I',)
isolatable_states = ('S', 'E', 'A', 'B', 'P', 'M', 'I',)
# Sets of states to do with control and isolation policies
known_control = {'C',}
known_iso_control = {'Q'}
test_iso_control = {'D', 'W'}
at_home_control = known_iso_control.union(test_iso_control)
unbound_control = {'U',}
unknown_control = unbound_control.union(at_home_control)
all_control = known_control.union(unknown_control)
# Other states to use in timeseries
dead_keys = {'DX'}
recovered_keys = {'RU', 'RT'}
hospital_keys = {'HQ', 'CQ'}
stale_keys = dead_keys.union(recovered_keys)
safe_keys = {'S'+control for control in unknown_control}.union(stale_keys)
unbounded_keys = state_prod(risky_states, unbound_control)
ub_infectious_keys = {s+"U" for s in testable_states}
active_keys = state_prod(risky_states, unknown_control).union(state_prod(testable_states, known_control)).union(hospital_keys)
selfiso_keys = {s+'Q' for s in isolatable_states}.union({s+'D' for s in isolatable_states}).union({s+'W' for s in isolatable_states})
traced_keys = {s+'Q' for s in isolatable_states}
confirmed_keys = {s+'C' for s in testable_states}.union(hospital_keys)
known_keys= confirmed_keys.union({'RT', 'DX'})
known_keys_not_RT = confirmed_keys.union({'DX'})
cumul_hosp_keys = hospital_keys.union({'RT', 'DX'})
cumul_crit_keys = {'CQ'}.union({'RT', 'DX'})
known_active_keys = known_keys.intersection(active_keys)
cumulative_keys = active_keys.union(stale_keys)

# define transition types
# with respect to transition strings
transitions = {}

close_groups = set(cobin._inverse_close_groups.keys())
# removes XZ groups from casual groups
casual_groups = set(cobin._inverse_all_groups.keys()).difference(close_groups)

def digitise_days(df):
    """ Converts t column in df to integer days 
        
    Args:
      df (pandas.DataFrame): dataframe to digitise
    """
    if 'day' in df:
        return df
    return df.assign(day = df.t.astype(int))

def shift_days(df, day):
    """ Shifts the digitised days by a certain number
        
    Args:
      df (pandas.DataFrame): dataframe whose days need to be shifted
      day (str): number of days to shift the timeseries by
    """
    return df.rename(lambda x: int(x) - int(day))

def byday_reindex(df, method='zfill'):
    """ Reindexes and fills the timeseries using one of two methods
        
    Args:
      df (pandas.DataFrame): timeseries to reindex
      method (str): either zfill or ffill, the method to reindex and fill the timeseries by
    """
    if method == 'zfill':
        return df.reindex(np.arange(0, int(max(df.index, default=0)+1)), fill_value=0)
    elif method in ['pad', 'ffill']:
        return df.reindex(np.arange(0, int(max(df.index, default=0)+1)), method='ffill')
    else:
        warnings.warn(f"Unknown fill method: {method}. Returning as-is.")
        return df

def fix_rq(df):
    """ Fixes RT and RQ values. NB: do not use unless you have first investigated the issue in the cobin repo """
    warnings.warn("This fix should only be applied to RQ-bug data")  
    df.loc[:,'RT'] += df.eto.eq('RQ').cumsum().to_numpy()
    df.loc[df.eto.eq('RQ'), 'eto'] = 'RT'