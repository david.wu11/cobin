import sys
from os import sep
import pandas as pd

if __name__ == "__main__":
    ofl = sys.argv[1]
    with open(ofl) as fl:
        data = fl.read().strip().split("\n")
    
    outname = ".".join(ofl.split(".")[:-1]) + ".csv"

    if len(sys.argv) > 2:
        arglabel = sys.argv[2]
    else:
        arglabel = "unknown metric quantity"

    Nln = 5

    infos = []
    for i in range(0, len(data), Nln):
        frag = data[i:i+Nln]
        doc = frag[0].split("\\")[-1].split("_")[-1]
        info = {
            'config': int(doc),
        }
        for i, v in enumerate(['mean', 'median', 'LQ', 'UQ']):
            info[v] = float(frag[i+1].split(":")[-1])
        infos.append(info)

    df = pd.DataFrame.from_records(infos)
    df = df.rename(columns={i:(i+ arglabel if i != 'config' else i) for i in df})
    df.sort_values('config').to_csv(outname, index=False)