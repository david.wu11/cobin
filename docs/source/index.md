# Welcome to networkcontagion!
`networkcontagion` is a `Python` library for simulating spreading processes on bipartite networks, with dynamics defined by reactions between individual agents on the network. It includes the Contagion On a Bipartite Interaction Network (`cobin`) model that was primarily used fo the modelling of COVID-19 in Aotearoa (the actual model employed by `networkcontagion`), and other modules related to the initialisation of and processing of model results.

## What is the cobin model?
`cobin` implements an exact version of Gillespie's algorithm, based on the direct method, with semi-Markovian extensions using delayed effects (a queue) and rejection-based simulation methods.

While developed specifically for the context of COVID-19 in Aotearoa, `cobin` models the spread of relatively generic processes that others can use to model infectious disease spread in other contexts.

`cobin` models contagion on a *bipartite network* which allows us to model spread through an underlying network of contacts, with different transmission probabilities and disease progression by context type and individual demographic factors. This is an advantage over models that assume a well-mixed, homogeneous population, as it allows us to to track fine-grained behaviour of epidemic spread.

The processes that `cobin` models include:
- non-symptomatic infection (pre-symptomatic and fully asymptomatic pathways), as well as symptomatic infection
- severe outcomes (hospitalisation, critical care, and death)
- spontaneous (symptom-driven) testing
- self-isolation, contact tracing, and induced testing
- non-pharmaceutical interventions (NPIs), i.e. lockdowns and restrictions
- multi-dose vaccination
- reinfection 
- waning immunity
- individual and context heterogeneity

## Uses of networkcontagion
The `networkcontagion` library was developed in part to model the spread of COVID-19 throughout Aotearoa between 2020 and 2023. It forms the basis of the Network Contagion Model used by [COVID-19 Modelling Aotearoa](https://www.covid19modelling.ac.nz/) (CMA).

Some publications that use the results of this model are:
- [COVID-19 network modelling trilogy: Elimination, Alert Level 2.5 and other non-pharmaceutical interventions](https://www.covid19modelling.ac.nz/network-modelling-trilogy/)
- [Contagion network modelling in the first weeks of the August 2021 outbreak](https://www.covid19modelling.ac.nz/contagion-network-modelling-in-the-first-weeks-of-the-august-2021-outbreak/)

## Key references
The [Epidemics on Networks (EoN)](https://github.com/springer-math/Mathematics-of-Epidemics-on-Networks) Python package formed the basis for our initial contagion model, but we have significantly customised and extended this package to allow for additional functionality in our modelling.  Some of these modifications are inspired by similar models and simulation frameworks developed recently for the spread of COVID-19 in other countries.

A brief, incomplete list of modifications/extensions is as follows:

- Added events that have actions after some known delay, with reference to [Dmitri et al](https://doi.org/10.1117/12.609707) and [Anderson](https://doi.org/10.1063/1.2799998)
- Changed the behaviour of how the system state is recorded so that "non-events" are not recorded (they are now "shadow events"). This is useful for the rejection-based Kinetic Monte Carlo formulations that are employed.
- Added additional recorded information in the form of actor nodes, auxiliary node information, and transition information
- Added capability for mutation-type events which explicitly allow changing nodes that are not the node of interest.

## How to get started
For instructions on how to install go to [Installation](install.md)

Once you have completed installation, we have included a tutorial for how to run an example contagion process on an example network and analyse basic outputs. [Go to Tutorial](install.md)

## Support and information for developers
This library is currently a snapshot of the code for the Network Contagion Model - one of the models that [COVID-19 Modelling Aotearoa](https://covid19modelling.ac.nz/) has used to model the spread of COVID-19 in Aotearoa over the past few years. 

We hope that it will serve as a basis for others wanting to model spread on bipartite networks. While further development may occur in the future, at this point we are not planning to actively maintain or develop it further. 

If you are interested in doing so however, please see the license for the package's copyright holders and contact authors.

## How this documentation is structured
This documentation covers both the conceptual network contagion model, its implementation in the `networkcontagion` library.

The *conceptual model* sections include the details behind the primary model that was used for the spread of COVID-19 in New Zealand, the modelling assumptions made, process flow within the model, parameters and inputs and features included within the model package. It finishes with an outline of the main algorithms/methods used to simulate the model.

The *model implementation* sections outline the specifics of how the model code works, and what the inputs for each simulation run include. We also include module and function descriptions for the `networkcontagion` library.

## Contributors
The library was developed by a collection of researchers at [COVID-19 Modelling Aotearoa](https://www.covid19modelling.ac.nz/) over the period from February 2020 to August 2023. The development team includes, in approximate chronological order: Demival Vasques Filho, Dion O'Neale, Steven Turnbull, Adrian Ortiz Cervantes, Emily Harvey, Oliver Maclaren, David Wu, Frankie Patten-Elliot, Josh Looker, Joel Trent, Gray Manicom, Ella Priest Forsyth.

David Wu in particular has contributed the vast majority of the code in this library - see his PhD thesis, supervised by Oliver Maclaren, for more details on simulating Markovian processes.

Thanks to many others in the CMA team and the University of Auckland Centre for eResearch for assistance and to Joel Miller for the EoN package and accompanying book and documentation that this work is based on. 

```{toctree}
:caption: 'Installation'
:maxdepth: 2
prereqs
install
dependencies
```

```{toctree}
:caption: 'COBIN Model'
:maxdepth: 2

conceptual_overview
implementation_overview
networkcontagion module and function descriptions <modules>
license_link
```
```{toctree}
:caption: 'Sample input values and networks included'
:maxdepth: 1

sample_values_refs
```

```{toctree}
:caption: 'Tutorial'
:maxdepth: 1

tutorial
```

```{toctree}
:caption: 'FAQ and Common Errors'
:maxdepth: 1

faq
```
