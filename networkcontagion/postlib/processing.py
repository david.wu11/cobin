"""File, directory, command line parsing and main control logic"""
from typing import Any, Tuple

import functools
import argparse
import warnings
import glob
import multiprocessing
import numpy as np
import pandas as pd
import csv
import os.path
import re

from . import utils
from . import meta
from . import record_table
from .parse_yaml import read_config
from . import metrics



def select_metrics(metric_name: str, arg_str: str, metric_dict: dict):
    """ Select metrics from command line arguments 
    
    Args:
      metric_name (str): metric name to get help for
      arg_str (str): whether to calculate selection or all metrics, or print help message
      metric_dict (dict): dictionary of metrics to calculate
    """
    selected_keys = set()
    if arg_str == 'help':
        print(f'{metric_name} metrics choices: {metric_dict.keys()}')
        # no selection
    elif arg_str == '*':
        # select all
        selected_keys = metric_dict.keys()
    else:
        # sub-selection specified by the user
        selected_keys = eval(arg_str)
        selected_keys = set(selected_keys)
        # filter out invalid keys
        selected_keys &= metric_dict.keys()

    return {k: metric_dict[k] for k in selected_keys}
  

def import_file(filename: str, fix_rq: bool=False) -> Tuple[dict, pd.DataFrame]:
    """ Imports a npz file to numpy.NpzFile and pandas.DataFrame formats 
    
    Args: 
      filename (str): npz file to load
      fix_rq (bool): whether to fix the RQ state (DEVELOPMENT ONLY)
    """
    with np.load(filename) as data:
        dict_data = dict(data)
        df = pd.DataFrame.from_dict(dict_data)
        df = utils.digitise_days(df)
        if fix_rq:
            utils.fix_rq(df)
    return dict_data, df

def process_file(filename, columns, timeseries, config, record_table, record_table_timeseries, shift=False, warn=True, fix_rq=False):
    """ Get metrics and timeseries for a single npz file
    
    Args:
      filename (str): npz file to process
      columns (dict): dictionary of numerical metrics to compute
      timeseries (dict): dictionary of timeseries to output
      config (str): config YAML for the npz
      record_table (record_table.RecordTable): RecordTable of existing results
      record_table_timerseries (record_table.RecordTable): RecordTable of existing timeseries
      shift (bool): whether to shift the timeseries by time of detection
      warn (bool): whether to display function warnings
      fix_rq (bool): whether to fix the RQ state (DEVELOPMENT ONLY)
    """
    if not warn:
        warnings.filterwarnings('ignore')

    run = meta.get_name(filename)
    fn = os.path.basename(filename)
    rcd = record_table.get_record(fn)

    data, df = None, None
    output = {'filename': run}
    read_config(config, output)

    for column_name, column_call in columns.items():
        # either retrieve or compute if column_name is not in the record
        if column_name in rcd:
            output[column_name] = rcd[column_name]
        else:
            if data is None:
                data, df = import_file(filename, fix_rq=fix_rq)
            output[column_name] = column_call(data, df, output)

    for timeseries_name, timeseries_call in timeseries.items():
        # either retrieve or compute if timeseries_name is not in the record
        rcdt = record_table_timeseries.get_timerecords(fn, timeseries_name)
        if rcdt is not None:
            output[timeseries_name] = rcdt
        else:
            if data is None:
                data, df = import_file(filename, fix_rq=fix_rq)
            output[timeseries_name] = timeseries_call(data, df, output)

    # filter out any auxiliary information left in output
    filtered_output = {k:v for k,v in output.items() if k in columns}
    filtered_series_output = {k:v for k,v in output.items() if k in timeseries}

    if shift:
        if "Time to Detection" in output:
            dt = output['Time to Detection']
        else:
            if data is None:
                data, df = import_file(filename)
            dt = metrics.detection.time_of(data, df, output)
        if dt is not None:
            for k, v in filtered_series_output.items():
                filtered_series_output[k] = utils.shift_days(v, dt)

    return run, filtered_output, filtered_series_output

def process_directory(directory, config, columns, timeseries, parallel=False, shift_days=False,
                      warn=True, record_table=None, record_table_timeseries=None, fix_rq=False):
    """ Get metrics and timeseries for all npz files in a directory 

    Args:
      directory (str): directory of npzs to process
      config (str): config YAML for the directory
      columns (dict): dictionary of numerical metrics to compute
      timeseries (dict): dictionary of timeseries to output
      parallel (bool): whether to postprocess the npzs in parallel
      shift_days (bool): whether to shift the timeseries by time of detection
      warn (bool): whether to display function warnings
      record_table (record_table.RecordTable): RecordTable of existing results
      record_table_timeseries (record_table.RecordTable): RecordTable of existing timeseries
      fix_rq (bool): whether to fix the RQ state (DEVELOPMENT ONLY)

    Returns:
      out_df (pandas.Dataframe): dataframe output statistics for each npz in the directory
      ts_df (pandas.Dataframe): dataframe of timeseries specified for each npz in the directory
    """
    files = glob.glob(directory + "/*.npz")
    process_file_fn = functools.partial(process_file, 
                                        columns=columns,
                                        timeseries=timeseries,
                                        config=config,
                                        shift=shift_days,
                                        warn=warn,
                                        record_table=record_table,
                                        record_table_timeseries=record_table_timeseries,
                                        fix_rq=fix_rq,
                                        )
    if parallel and parallel > 1:
        pool = multiprocessing.Pool(processes=parallel)
        # computes process_file(file, columns) in parallel
        outputs = pool.map(process_file_fn, files)
        out_df = pd.DataFrame.from_dict({x[0]: x[1] for x in outputs}).T
        ts_df = pd.DataFrame.from_dict({(x[0], k): v for x in outputs for k,v in x[2].items()}).T
    else:
        outputs = dict()
        series_outputs = dict()
        for fl in files:
            name, output, series_output = process_file_fn(fl)
            outputs[name] = output
            for so_key, so_val in series_output.items():
                series_outputs[(name, so_key)] = so_val

        out_df = pd.DataFrame.from_dict(outputs).T
        ts_df = pd.DataFrame.from_dict(series_outputs).T

    return out_df, ts_df

def output_csv(outname, outputs):
    """ Saves a csv of given numerical metrics
    
    Args: 
      outname (str): name of csv to save
      outputs (pandas.DataFrame): dataframe of numerical metrics to save
    """
    outputs.index.name = 'run'
    outputs.to_csv(outname, quoting=csv.QUOTE_NONNUMERIC)

def output_timeseries(outname, series_outputs):
    """ Saves a csv of given timeseries
    
    Args: 
      outname (str): name of csv to save
      series_outputs (pandas.DataFrame): dataframe of timeseries to save
    """
    series_outputs.index.names = ['run', 'metric']
    series_outputs.to_csv(outname, quoting=csv.QUOTE_NONNUMERIC)

def plot_directory(outname_root, plot_type, series_outputs, plots):
    """ Plots the timeseries of a given directory
    
    Args: 
      outname_root (str): path of root output directory
      plot_type (str): type of file to save plots as
      series_outputs (pandas.DataFrame): dataframe of timerseries ot save
      plots (dict): dictionary of plots to save
    """
    for plt_suffix, pltfn in plots.items():
        pltfn(series_outputs, outname_root, plot_type, plt_suffix)

def parse_args(doc="Postprocessing"):
    """ Parses command line arguments to run_postprocessing/postprocessing_cobin.py """
    parser = argparse.ArgumentParser(description=doc)
    parser.add_argument('directory', help="directory that contains the npz outputs")
    parser.add_argument('-f', '--config-file', dest='config_file', default='configs/default.yaml',
                        help='path to yaml config used for sims (default.yaml if not provided)')
    parser.add_argument('-c', '--cores', dest='cores', default=1, type=int, metavar="N",
                        help='number of cores to use in parallel')
    parser.add_argument('--csv', action='store_true',
                        help='whether to create output files')
    parser.add_argument('-o', '--output', dest='outname', default=None, metavar="PATH",
                        help='(base) name of the output csv files (implies --csv)')
    parser.add_argument('-t', '--timeseries', dest='tsname', default=None, metavar='PATH',
                        help='(base) name of the timeseries output csv file (implies --csv)')
    parser.add_argument('-x', '--dry', '--dry-run', dest='dry', action='store_true',
                        help='print argument values and quit')
    parser.add_argument('-p', '--plot', dest='plot', action='store_true',
                        help='whether or not to generate and save plots')
    parser.add_argument('-n','--nametype', dest='plot_type', default='pdf', metavar='PATH',
                        help='type of file to save plots as e.g. pdf')
    parser.add_argument('-d', '--plot-directory', dest='plotout', default='.', metavar="DIRPATH",
                        help='path to directory to save plots to (defaults to current directory)')
    parser.add_argument('-w', '--warnings', action='store_true',
                        help='whether or not to see warnings')
    parser.add_argument('-C', '--column_metrics', type=str, dest='column_metrics', default='*',
                        help='List of column metrics to produce from keys, "help" to see available list, "*" to choose all')
    parser.add_argument('-T', '--timeseries_metrics', type=str, dest='timeseries_metrics', 
                        default='*', 
                        help='List of time series metrics to produce from keys, "help" to see available list, "*" to choose all')
    parser.add_argument('-P', '--plot_metrics', type=str, dest='plot_metrics', default='*', 
                        help='List of plot metrics to produce from keys, "help" to see available list, "*" to choose all')
    parser.add_argument('-s', '--shift-days', dest='shift_days', action='store_true',
                        help='whether or not to shift timeseries with respect to detection time')
    parser.add_argument('-F', '--force', action='store_true', dest='force', 
                        help='Whether to force the column metrics to be recomputed')
    parser.add_argument('--fix-rq', action='store_true', dest='fix_rq',
                        help='[DANGEROUS] Whether to fix the RQ bug')

    args = parser.parse_args()

    if args.dry:
        import pprint
        pprint.pprint(vars(args))
        parser.exit(0) # system exit

    return args

def run_postprocessing(columns: dict=None, timeseries: dict=None, plots: dict=None, **kwargs):
    """ Runs the postlib for given arguments
    
    Args:
      columns (dict): Metric columns to include (defined in metrics.py)
      timeseries (dict): Timeseries to include (defined in metrics.py)
      plots (dict): Plots to save (defined in metrics.py)
    """   
    # If no columns, timeseries or plots passed then initialise
    if columns is None:
        columns = dict()
    if timeseries is None:
        timeseries = dict()
    if plots is None:
        plots = dict()

    # parse the arguments and create the outputs directory
    args = parse_args(**kwargs)
    clean_name = meta.clean_dir_path(args.directory)

    # Initialise outputs as empty if not specified
    if args.outname is None and not args.csv:
        columns = dict()
    if args.tsname is None and not args.csv:
        timeseries = dict()
    if args.plotout is None and not args.plot:
        plots = dict()

# Build dictionary of stats/series/plots asked for and the functions to produce them
    cs_dict = select_metrics('column', args.column_metrics, columns)
    ts_dict = select_metrics('timeseries', args.timeseries_metrics, timeseries)
    ps_dict = select_metrics('plot', args.plot_metrics, plots)

    # set the CSV file name
    outname = None
    if args.csv or args.outname is not None:
        if args.outname is None:
            outname = f'outputs_{clean_name}.csv'
        else:
            outname = args.outname

    # build the record table from already stored results
    rcrd = record_table.RecordTable()
    if outname:
        if os.path.exists(outname) and not args.force:
            rcrd.read_csv(outname)

    # set the CSV time series file name
    outname_timeseries = None
    if args.csv or args.tsname is not None:
        if args.tsname is None:
            outname_timeseries = f'timeseries_{clean_name}.csv'
        else:
            outname_timeseries = args.tsname

    # build the time series record table from already stored results
    rcrd_timeseries = record_table.RecordTable()
    if outname_timeseries:
        if os.path.exists(outname_timeseries) and not args.force:
            rcrd_timeseries.read_csv(outname_timeseries)

    # Process the npz directory
    outputs, series_outputs = process_directory(args.directory,
                                    config=args.config_file,
                                    columns=cs_dict,
                                    timeseries=ts_dict,
                                    parallel=args.cores,
                                    shift_days=args.shift_days,
                                    warn=args.warnings,
                                    record_table=rcrd,
                                    record_table_timeseries=rcrd_timeseries,
                                    fix_rq=args.fix_rq,
                                    )

    # write the CSV results
    if outname is not None:
        output_csv(outname, outputs)

    # write the time series results
    if outname_timeseries is not None:
        output_timeseries(outname_timeseries, series_outputs)
        
    # generate and save plots
    if args.plot and ps_dict:
        outname_root = args.plotout
        plot_type = args.plot_type
        plot_directory(outname_root, plot_type, series_outputs, plots=ps_dict)
