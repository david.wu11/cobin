# Default Yamls

Here we include a number of default yaml files. Each yaml is arranged into 7 distinct sections:
- INITIALISATION
- CONTAGION PARAMETERS
- VACCINATION
- TESTING
- TRACING/ISOLATING
- CLOSE CONTACTS
- INTERVENTIONS

In each of the new default yaml files, we define only e.g. the CONTAGION PARAMETERS, and use dummy values for the rest. This should allow us to easily grab e.g. the contagion parameters for delta variant without missing parameters etc. So the new default yamls are:
- `default_wildtype.yaml` - defines the CONTAGION PARAMETERS and VACCINATION parameters as those corresponding to wildtype COVID
- `default_delta.yaml` - defines the CONTAGION PARAMETERS and VACCINATION parameters as those corresponding to delta variant
- `default_omicron.yaml` - defines the CONTAGION PARAMETERS and VACCINATION parameters as those corresponding to omicron variant
- `default_TTI.yaml` - defines the TESTING,  TRACING/ISOLATING and CLOSE CONTACTS parameters as default 2022 values.
- `default_AL.yaml` - defines the INTERVENTIONS parameters as AL2/AL3/AL4.
- `default_TTI.yaml` - defines the INTERVENTIONS parameters as GREEN/ORANGE/RED.

## Usage

For a simulation to run, all parameters need to be defined (i.e. we need to fill out the dummy values). To create a yaml for use in simulation, make a copy of one of these base default yaml files (e.g. `default_omicron.yaml` if you wish to run a simulation with omicron behaviour), and then either fill out the dummy values with parameters of your choice, or replace the necessary sections with the default parameter sections from the appropriate default yaml.