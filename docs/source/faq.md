# FAQ and Errors
This page contains a selection of questions, errors and fixes that we, the CMA team, encountered ourselves when attempting to install and run the `networkcontagion` library. It is by no means an exhaustive list and please look elsewhere in the documentation if you encounter an issue or a question that is not covered below (or feel free to contact the team).

## Installation and Documentation
The most common issue that can arise during installation is using an outdated version of Python. By default, `conda` does not use Python 3.10+ and so care will have to be taken to setup your environment to use a more recent Python version. If you are installing Python through another method, please do still install Python 3.10+ to also avoid any related errors.

As stated in the [install page](install.md), there are various terminal and OS specific problems that can occur when installing the documentation. Most of them are sortable by using different quotation marks and escape characters to ensure the shell or terminal reads in the commands properly.

If you make additions or modifications to the codebase and wish to rebuild the automatic documentation, ensure that you run both `make gen` and `make docs` to force `sphinx` to recreate the Python autodocs and create any new ones.

## Running
A common error that can be found when running the `cobin` model is a `KeyError`. These are normally found when a parameter name or value has been set incorrectly, omitted or misnamed within the configuration yaml and the exact parameter can normally be discerned by reading where the `KeyError` occurred.

Although this is less likely to be an issue for 'local' instances of the `networkcontagion` library, there have been instances where a job run on NeSI was cancelled due to running out of memory or computation time. This would only happen for runs set for a very long period of time (in the range of 6+ months) and/or on a very large network (such as the whole of New Zealand). This should never happen for the provided Tutorial/Vignette configurations and simulations run over similar input networks/simulation windows, especially when run on infrastructure that does not impose limits on jobs pre-running. 

Another common error is that I/O paths cannot be accessed by the library. From our experience, this is most likely caused by forgetting or misplacing a separator (i.e. '/') in the file paths specified or slightly misnaming a network object/configuration yaml.

Different rate or infection parameters can take different forms (e.g. a double for `alpha_sr` and an age-dependent tuple for `beta_0`) and it can be hard determine (without reference to the code, documentation and/or experience) which parameters take different forms. This is especially true for the vaccination parameters (which are almost all age-dependent tuples). We recommend using the reference configurations provided in the `configs` directory as a guide, and also making use of the ability within the Config Loader to define custom 'tuple lists' for repeated values (such as the `*ones,*testing,*hosp,*mort` lists in `configs/vignette_config_vaccination.yaml`).

A user error that’s very easy to make (especially for new users) is thinking a parameter value is X (for example `p_test_i=0.75` in `configs/vignette_config_vaccination.yaml`), but then it is reset to a different value Y during an adaptive lockdown (for example `p_test_i=0.5` in the `high_lock` lockdown setting in `configs/vignette_config_vaccination.yaml`).