Current data: 11 Nov 2022.

These vaccine parameters are chosen to describe protection against BA.4 and BA.5 subvariants of the omicron variant due to vaccination and/or previous infection. 

Note that
1) The protection from previous infection assumes previous infection with omicron BA.1 or omicron BA.2 subvariants. 
2) There was not that much real-world data when the target VE was calculated, so these should be re-checked at some point. 
3) This was before the SAR calculations were finalised, so protection against acquisition may change. 