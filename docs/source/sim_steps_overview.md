# Internal Processes

Here, we describe the internals of the `Simulation` interface, and the rest of the `networkcontagion` library.

## Overview

```{mermaid}

flowchart LR
   subgraph loader [Config Loading]
    direction LR
      conf[(Config File)] --> cl([config_loader]) --> config{{Config}}
   end
   subgraph gload [Graph Loading]
    direction LR
      grph[(Graph File)] --> gh([graph_handler]) --> nxg{{Networkx<br/>Graph}}
    end
   model{{Model<br/>Libraries}}
   subgraph simx [Simulation Object]
      direction LR
      rndm[Set random seed]
      Parameters -->|init| Sim_Objects
      nxg --> ghmirror[Graph]
      config --> configmirror[Config]
      initer[Initial<br/>Condition<br/>Generator]
   end
   model --> Parameters & initer
   rstrt[(Restart<br/>State)] -.-> simx
   subgraph gill [Simulation Algorithm]
    ccloop([gillespie_max])
   end
   simx --> gill

```



## `Simulation` initialisation

Run when the `Simulation` is created

1. Initialise the ``sim_objects`` using the ``parameters``
2. Filter and sort the ``parameters``
3. Load the graph using `graph_handling`
4. Extract the ``run_options`` from the config
5. Set the random seed
6. Check for a state to restart from
7. Build the initial condition constructor and cache the reseeding function

## `Simulation.run` initialisation

Run when a single run/realisation is requested

1. Generate the initial conditions
1. Extract or set default function for simulation functions
2. Prepare for restarting from a specified state, if given
3. Re-initialise the ``sim_objects``
4. Set up the event queue

## `gillespie_max` loop

While there are active reactions or there are still queued events, and we have not yet infected the maximum number of cases:
   1. Lookup ``nodes_by_rate``[`ListDict`] maximum rate, draw time to next event
   2. While there are any events in the ``event_queue`` that would fire first:
      1. Set the current time to the time of the event that would fire
      2. Call `manage_event` to realise event (state changes) and get influence set
      3. Record state changes in ``records``[`ContagionRecords`]
      4. Recompute rates of nodes in the influence set (update ``nodes_by_rate``) using the ``rate_function``.
      5. Generate a time to new event, using ``nodes_by_rate``
   3. Update the current time to the time of the new event
   4. Check if the time has reached the maximum time
      1. If so, return.
   5. Draw a node that is reacting from ``nodes_by_rate``
   6. Draw the type of reaction occurring using ``transition_choice``
   7. Call `manage_event` to realise event (state changes) and get influence set
   8. Record state changes in ``records``[`ContagionRecords`]
   9. Recompute rates of nodes in the influence set (update ``nodes_by_rate``) using the ``rate_function``.
**Output** ``records``.

## Post simulation actions

Run after the `gillespie_max` loop has terminated

1. Write the results out to disk
2. Optionally write the current configuration and current internal state to disk
3. Perform cleanup on the graph

See {doc}`outputs` for more information.
