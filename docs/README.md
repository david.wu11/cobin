# networkcontagion Docs

This is meant for development purposes. A built version of these docs should be available.

## Building the Docs

For development on the docs, you should install the extra dependencies for building the docs in the parent directory of this file:

```
pip install networkcontagion[docs]
```
**Note:** if you are using the `zsh` shell, '[]' are used to denote wildcards and thus the above command should be edited to instead include: `'[docs]'`


The docs can be built with

```
make docs
```

from this directory.


**Note:** if you have installed `make` through Anaconda, and have not added the executable to your system path you will need to open an Anaconda Prompt and activate the environment that you have installed `make` into. 
For *NIX users you can then run the commands stated as is, but Windows users will instead have to replace references to `make` with `mingw32-make`

### Detailed Build Process

Some of the documentation is auto-generated. We do this autogeneration with `sphinx-apidoc`. This is automated as

```
make gen
```

This generates additional documents in the `source` directory.

After generating the API docs, we can build the docs using

```
make docs
```

This creates the readable documentation as html files in the `build` directory, which can then be deployed.

Alternative format targets can be invoked using

```
make [format]
```

where `[format]` is a an output format supported by Sphinx (for example, latexpdf).

---
Currently, we only guarantee successful output to html - this is partially due to the dependency on the sphinxcontrib-pseudocode extension, which is built specifically for html. We anticipate latex support to be added in the near future though.

### Note on sphinxcontrib pacakges

There seems to be a silent conflict between sphinxcontrib packages, where they share an `exceptions.py` file. This can cause problems when using multiple packages with each other. This can be manually fixed by editing the `exceptions.py` file to include the overwritten exceptions.