""" Handling of networkx objects and toy network generation """

import networkx as nx
import matplotlib.pyplot as plt
from functools import reduce 
from itertools import chain, islice
from collections import defaultdict
import numpy as np
import pickle

class GraphLoadException(AssertionError):
    pass

def load_from_pickle(filename):
    """Loads graph from gpickle file
    Args:
      filename: path to gpickle file
    Returns:
        G: loaded gpickle
    """
    with open(filename, 'rb') as pickle_file:
        G = pickle.load(pickle_file)
    if nx.is_frozen(G):
        raise GraphLoadException(f"{filename} contains a frozen graph, which is not suitable for this simulation.")
    return G

def get_bipartite_parts(G, type='individuals', status_func=None, status_type=None):
    """Loads graph from gpickle file
    Args:
      G (nx graph): Graph to analyse
      type (string): type of node
      status_func (list): node status to check 
      status_type: status of node to include
    Returns:
        G: loaded gpickle
    """
    # note: individuals 1, groups 0
    # options - individuals, groups, both
    if status_func is not None:
        if type=='individuals':
            return [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 1 and status_func[ni] == status_type]
        elif type=='groups':
            return [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 0 and status_func[ni] == status_type]
        elif type=='both':
            individuals = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 1 and status_func[ni] == status_type]
            groups = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 0 and status_func[ni] == status_type]
            return individuals, groups
        else: # for now
            individuals = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 1 and status_func[ni] == status_type]
            groups = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 0 and status_func[ni] == status_type]
            return individuals, groups
    else: # no filter
        if type == 'individuals':
            return [ni for ni, bi in G.nodes(data='bipartite') if bi == 1]
        elif type == 'groups':
            return [ni for ni, bi in G.nodes(data='bipartite') if bi == 0]
        elif type == 'both':
            individuals = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 1]
            groups = [ni for ni, bi in G.nodes(data='bipartite') if bi == 0]
            return individuals, groups
        else:  # for now
            individuals = [ni for ni, bi in G.nodes(
                data='bipartite') if bi == 1]
            groups = [ni for ni, bi in G.nodes(data='bipartite') if bi == 0]
            return individuals, groups
    return 


def get_neighbours_based_on_groups(G, node, label_by_groups=False):
    '''
    Custom implementation of definition of 'neighbour' defined as any node belonging to one or more shared group.

    '''
    if label_by_groups==False:
        # -get all group nodes for given node
        node_groups = (g for g in G[node])
        # # -get a list of lists of individual neighbours (one list for each group node)
        # node_neighbours_list_of_lists = [list(G[g]) for g in node_groups]

        # # -flatten list of lists into one big list of neighbours. A few ways to do below. Need to remove self though!
        # #reduce(lambda a, b: a+b, node_neighbours_list_of_lists, []) #then remove self...
        # #list(chain.from_iterable(node_neighbours_list_of_lists)) #then remove self...
        # node_neighbours = [
        #     i for sublist in node_neighbours_list_of_lists for i in sublist if i != node]
        # return node_neighbours

        # return all neighbours of neighbours
        return (i for g in node_groups for i in G[g] if i != node)
    else: 
        # -get all group nodes for given node
        node_groups = (g for g in G[node])
        # # -get a list of lists of individual neighbours (one list for each group node)
        # node_neighbours_list_of_lists = [zip(list(G[g]),len(G[g])*[g]) for g in node_groups]

        # # -flatten list of lists into one big list of neighbours. A few ways to do below. Need to remove self though!
        # #reduce(lambda a, b: a+b, node_neighbours_list_of_lists, []) #then remove self...
        # #list(chain.from_iterable(node_neighbours_list_of_lists)) #then remove self...
        # node_neighbours = [
        #     i for sublist in node_neighbours_list_of_lists for i in sublist if i != node]
        return ((i, g) for g in node_groups for i in G[g] if i != node)

def get_neighbours_on_group_filter(G, node, group_filter, label_by_groups=True):
    """Gets neigbhours of a node from a a specific group type
    Args:
      G (nx graph): Graph to analyse
      node (nx node): Group node to get neighbours in
      group_filter (string): W,G,V,X,Z for close or casual groupings
    Returns:
        list of neighbours
    """
    if label_by_groups:
        return ((i,g) for g in G[node] if g[:2] in group_filter for i in G[g] if i != node)
    else:
        return (i for g in G[node] if g[:2] in group_filter for i in G[g] if i != node)

def generator_get(generator, index):
    return next(islice(generator, index, None))

def get_neighbours_flat(G, node):
    '''
    Not really necessary to define a whole function. Will use inline statement instead, but func here if needed.
    '''
    return list(G[node])

def make_test_graph(groups, total_num_individuals=10, all_in_g1=False, max_per_group=np.inf, include_isolated=False):
    '''
    for messing with...
    '''

    if max_per_group > total_num_individuals:
        max_per_group = total_num_individuals

    # edge list:
    # n in group 1, m in group 2, n+m >= total number (may be in both) = max(n,m)

    # random number for all
    num_per_group = np.random.randint(
        low=1, high=max_per_group+1, size=len(groups))

    # if all connected, assign all to first group
    if all_in_g1:
        num_per_group[0] = total_num_individuals

    #nodes_in_group = [0]*len(groups)
    e = []
    for i, g in enumerate(groups):
        if g == 'g1' and all_in_g1:
            nodes_in_group = list(np.arange(1, total_num_individuals+1))
            e = e + [(ni, g) for ni in nodes_in_group]
        elif num_per_group[i] > 0:
            nodes_in_group = list(np.random.choice(
                np.arange(1, total_num_individuals+1), size=num_per_group[i]))
            e = e + [(ni, g) for ni in nodes_in_group]

    # make graph
    G = nx.Graph()
    if include_isolated:  # add nodes with no edges to graph to track
        G.add_nodes_from(list(np.arange(1, total_num_individuals+1)))
    G.add_edges_from(e)

    return G

def project_real_bipartite_graph(G):
    '''
    assumes has bipartite property data: 

    1: individual 
    0: group

    '''

    G_flat = nx.Graph()

    # get individual nodes
    individual_nodes = [ni for ni, bi in G.nodes(data='bipartite') if bi == 1]
    # find neighbours for all individual and add arc.
    # question: are duplicates ignored?
    for ni in individual_nodes:
        # get_neighbours_based_on_groups
        neighbours = get_neighbours_based_on_groups(G,ni)

        for neighbour in neighbours:
            G_flat.add_edge(ni,neighbour)
    
    return G_flat


def create_all_to_one_bipartite_graph(G, include_isolated=False):
    '''
    Currently just makes attribute free version?

    Will need to update if we want to preserve attributes.

    Also currently doing in very dumb way...
    '''

    G_all_to_one = nx.Graph()
    G_all_to_one.add_node('EV-ALL', bipartite=0)

    individuals = [ni for ni, bi in G.nodes(data='bipartite') if bi == 1]

    if not include_isolated:
        individuals = [ni for ni in individuals if len(
            get_neighbours_based_on_groups(G, ni)) > 0]

    for ni in individuals:
        G_all_to_one.add_node(ni, bipartite=1)
        G_all_to_one.add_edge(ni,'EV-ALL')

    return G_all_to_one

def project_toy_bipartite_graph(G):
    '''
    assumes has 'g' as group label
    '''

    G_flat = nx.Graph()


    # get individual nodes
    individual_nodes = [ni for ni in G.node if str(ni)[0] != 'g']


    # find neighbours for all individual and add arc.
    # question: are duplicates ignored?
    for ni in individual_nodes:
        # get_neighbours_based_on_groups
        neighbours = get_neighbours_based_on_groups(G,ni)

        for neighbour in neighbours:
            G_flat.add_edge(ni,neighbour)
    
    return G_flat
