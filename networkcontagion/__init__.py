"""Simulation and postprocessing of stochastic contagion on a bipartite network

lib
---
    Simulation logic.
    Contains all code related to simulation - simulation config parsing, generic stochastic simulation logic,
    implementation of network contagion model.

postlib
-------
    Postprocessing logic.
    Contains logic to convert simulation output to parseable csv files with
    aggregate metrics.
"""
import os as _os

if any(_os.path.exists(_os.path.join(module_path, 'postlib')) for module_path in __path__):
    __all__ = ['lib', 'postlib']
else:
    __all__ = ['lib']

from . import *