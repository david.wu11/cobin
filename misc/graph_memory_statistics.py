#!/usr/bin/env python3

import networkx as nx
import pickle
import collections.abc as cabc
import sys
import argparse as ap
 
# Based on https://code.tutsplus.com/tutorials/understand-how-much-memory-your-python-objects-use--cms-25609
def deep_getsizeof(o, ids, totals, counts):
    """
    Find the memory footprint of a Python object
 
    Measure memory usage for object o via "getsizeof". If an object
    is a subclass of Mapping or Container (lists, sets, dicts, ...),
    the function will recursively drill down the object graph and count
    each contained object. Memory consumption and instance counts per
    object class are accumulated in totals and counts.

    Object IDs are registered to avoid counting objects twice.
    """

    d = deep_getsizeof

    # Return 0 byte if object has been counted already, otherwise count and register object
    if id(o) in ids:
        return 0

    r = sys.getsizeof(o)
    ids.add(id(o))

    # Update per-class memory consumption and instance counts
    typeName = type(o).__name__
    if typeName in totals:
        totals[typeName] += r
    else:
        totals[typeName] = r
    if typeName in counts:
        counts[typeName] += 1
    else:
        counts[typeName] = 1

    # Drill down into collections. Treat strings separately to count their memory size correctly
    if isinstance(o, str):
        return r
    elif isinstance(o, cabc.Mapping):
        return r + sum(d(k, ids, totals, counts) + d(v, ids, totals, counts) for k, v in o.items())
    elif isinstance(o, cabc.Container):
        return r + sum(d(x, ids, totals, counts) for x in o)

    return r

def get_graph_object_memsize(obj):
    """
    Compute memory consumption by traversing all members of obj (graph nodes
    or edges), including attached data structures
    """

    totalMem = 0
    typeTotals = dict()
    typeCounts = dict()

    barLength = 10
    memberCnt = 0
    numMembers = len(obj)
    oldCompleted = -1

    for member in obj:
        totalMem += deep_getsizeof(member, set(), typeTotals, typeCounts)

        # Show progress bar, count can take a long time
        memberCnt += 1
        completed = round(memberCnt/numMembers,2)
        bar = round(barLength*completed)
        if completed > oldCompleted:
            oldCompleted = completed
            barString = "\r{:s}[{}] {:4.0%} completed".format(" "*6, "*"*bar + " "*(barLength-bar), completed)
            sys.stdout.write(barString)
            sys.stdout.flush()

    print()
    return totalMem, typeTotals, typeCounts

def printInfo(msg):
    print("INFO: " + msg)

def printError(msg):
    print("ERROR: " + mfg)

def main():

    descStr = "Utility for computing memory consumption of networkx graph nodes and edges"
    parser = ap.ArgumentParser(description=descStr)
    parser.add_argument("filename", help="pickle file with a networkx graph")
    args = parser.parse_args()

    printInfo("Loading pickle file {}...".format(args.filename))
    with open(args.filename, 'rb') as pickle_file:
        G = pickle.load(pickle_file)

    if not isinstance(G, nx.Graph):
        printError("Pickled object is not a networkx graph.")
        return

    printInfo("Number of nodes: {} Number of edges: {}".format(G.number_of_nodes(), G.number_of_edges()))

    printInfo("Analyzing nodes...")
    nodeMem, nodeTotals, nodeCounts = get_graph_object_memsize(G.nodes(data=True))

    printInfo("Found {} Byte ({:7.1f} MiB) used for nodes".format(nodeMem, nodeMem/1024**2))
    print("{:s}Breakdown by node object type:".format(" "*6))
    for nodeObjType in nodeTotals:
        print("{:s}{:8s}: {:12d} Byte ({:7.1f} MiB) for {:12d} instances".\
            format(" "*6, nodeObjType, nodeTotals[nodeObjType], nodeTotals[nodeObjType]/1024**2, nodeCounts[nodeObjType]))

    printInfo("Analyzing edges...")
    edgeMem, edgeTotals, edgeCounts = get_graph_object_memsize(G.edges(data=True))

    printInfo("Found {} Byte ({:7.1f} MiB) used for edges".format(edgeMem, edgeMem/1024**2))
    print("{:s}Breakdown by edge object type:".format(" "*6))
    for edgeObjType in edgeTotals:
        print("{:s}{:8s}: {:12d} Byte ({:7.1f} MiB) for {:12d} instances".\
            format(" "*6, edgeObjType, edgeTotals[edgeObjType], edgeTotals[edgeObjType]/1024**2, edgeCounts[edgeObjType]))

if __name__ == "__main__":
    main()
