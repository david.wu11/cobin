#! /usr/bin/env python

# top-level class for interaction
from networkcontagion.lib import simulation

from networkcontagion.lib import cobin as cbn
from networkcontagion.lib import cobin_build as cbnbuild

if __name__ == "__main__":
    from sys import argv, exit as sysexit
    from networkcontagion.lib.config_loader import load
    import argparse, pathlib

    # parse command line arguments
    parser = argparse.ArgumentParser(description='Run networkcontagion cobin simulation')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('config_file', type=pathlib.Path)
    args = parser.parse_args()

    config_file = args.config_file
    verbosity = args.verbose

    # load in config file using config_loader module
    if verbosity:
        print(f"Loading config file {config_file}")
    config = load(config_file)

    # construct simulation
    sim = simulation.Simulation(config, cbn, cbnbuild, verbosity)

    # run and write results to disk
    sim.run()

    # report results
    results_str = sim.summarise()
    if verbosity:
        print(results_str)
