#######################################
# SET UP, RUN, AND INITIAL CONDITIONS #
graph: /absolute/path/to/network.gpickle
num_runs: 1
run_options:
  dump_internals: false
  tmax: 30
  full_return: true
  output_dir: /absolute/path/to/output_directory
  max_infections: 50000
  print_params: false
initial_conditions:
  random: true
  num_seeds: 5
########################
# CONTAGION PARAMETERS # 
parameters: 
# - Disease progression ---------------------------------
# --- Asymptomatic or mild infections
  p_asympt_list: !include '/cobin/configs/parameters_by_TA/p_asympt.csv'
  p_mild_list: !include '/cobin/configs/parameters_by_TA/p_mild.csv'
# --- Incubation period
  gamma:
    scale: 0.9
    shape: 4.1
# --- Symptom progression rates
  delta_bm: 0.85
  delta_pi: 0.85
  delta_ih: 0.2
  delta_hc: 0.5
# --- Recovery and fatality rates
  alpha_ar: 0.3
  alpha_mr: 0.1
  alpha_md: 0.1111111111111111
  alpha_ir: 0.3
  alpha_id: 0.1111111111111111
  alpha_hr: 0.2083333333333333
  alpha_cr: 0.0555555555555555
  alpha_cd: 0.1111111111111111
# --- mortality probability
  mu_m: 0
  mu_i: 0
# --- Hospitalisation, Critical Care and Infection Fatality probabilites
  mu_c_list: !include '/cobin/configs/parameters_by_TA/mu_c.csv'
  eta_h_list: !include '/cobin/configs/parameters_by_TA/eta_h.csv'
  eta_c_list: !include '/cobin/configs/parameters_by_TA/eta_c.csv'
# --- Reseeding
  reseed:
    - name: reseed_100
      time: 5
      random: True
      replay: 5 # Reseed again weekly
      only_if: ['SU', 'SQ', 'SW']
      num_seeds: 40 # approx 40 random individuals selected
      max_attempts: 1000000

# - Transmission ----------------------------
# --- Frequency vs density dependent infection settings
# For frequency-dependent infection settings: density: 1.0; density_groups: []; beta_h: all ages set to 0.0
  density: 1.0
  density_groups: []
  beta_h:
    - 0.0
    - 0.0
    - 0.0
    - 0.0
# --- Base infectivity
  beta_0:
    - 0.18
    - 0.18
    - 0.18
    - 0.18
  beta_c:
    - 1.8
    - 1.8
    - 1.8
    - 1.8
  beta_weights:
    - 1.0
    - 1.0
    - 1.0
    - 1.0
# --- Relative infectivity
  eps_a: 0.85
  eps_b: 0.85
  eps_m: 0.85
  eps_p: 0.85
  eps_h: 0
  eps_c: 0
# --- Transmission context weights
  group_type_weights:
    HOME: 1
    WORK: 2
    COMM: 1.33
  close_contact_weights:
    HOME: 2.5
    WORK: 1.0
    COMM: 4.0
# --- Transmission event parameters
  transmission_event_parameters:
    exponent: 0.2
    sigma: 0.1
    alpha: 1.1
    beta: 8.5
####################################
##### VACCINATION/REINFECTION ###### 
# - Reinfection ---------------------------------
  alpha_sr: 0.14
# - Vaccination ---------------------------------
  vaccination:
    date_offset: 0
    ? !()
    - 0
    - true
    : waning: !include '/cobin/configs/vaccine_efficacy/omicron/with_infection/infection_no_reinfection.yaml'
      txn_weight: &ones
        - 1
        - 1
        - 1
        - 1
      rcv_weight: *ones
      p_sympt: *ones
      p_mild: *ones
      testing: &testing
        p_test_0: 1
        p_test_m: 1
        p_test_i: 1
        theta_0: 1
        theta_m: 1
        theta_i: 1
      hospitalisation: &hosp
        eta_h:
          age:
            default: 1
          baseline: 1
        eta_c:
          age:
            default: 1
          baseline: 1
      mortality: &mort
        mu_m: 1
        mu_i: 1
        mu_c:
          age:
            default: 1
          baseline: 1
##############################################
####### TEST/TRACE/ISOLATE/QUARANTINE ######## 
#- Testing ---------------------------------
  p_test_0: 0
  p_test_m: 0
  p_test_i:
  - 0.6
  - 0.6
  - 0.6
  - 0.6
  theta_0: 0
  theta_m: 0.5
  theta_i: 0.8
  theta_lab: 0.5
  quarantine:
    p_test_0: 0.5
    p_test_m: 0.7
    p_test_i:
    - 0.75
    - 0.75
    - 0.75
    - 0.75
    theta_0: 0.5
    theta_m: 0.7
    theta_i: 1.2
  casual_test_prob:
    default: 0.1
    dw: 0.5
    dz: 0.5
    we: 0.5
    wp: 0.5
    wz: 0.5
    sc: 0.4
    sz: 0.4
    pm: 0.2
    pz: 0.2
    ev: 0.1
  casual_test_delay:
    core:
      alpha: 3
      beta: 5
    scale: 4.5
    loc: 2.5
#- Tracing ---------------------------------
  kappa_0:
    scale: 3
    shape: 2
  kappa_c: 4
  trace_weights:
    HOME:
      close: 0
      casual: 0.25
    WORK:
      close: 2
      casual: 0.25
    COMM:
      close: 2
      casual: 0.25
  notrace:
    dw: 0
    dg: 0
    dz: 0
    we: 1
    wp: 1
    wg:
      default: 1
      RED: 0.4
    wz:
      default: 1
      RED: 0.4
    sc: 1
    sg:
      default: 1
      RED: 0.4
    sz:
      default: 1
      RED: 0.4
    pm: 1
    pg:
      default: 1
      RED: 0.4
    pz:
      default: 1
      RED: 0.4
    ev: 1
    ex:
      default: 1
      RED: 0.4
  trace_drop:
    dw: 0.01
    dg: 0
    dz: 0
    we: 0.1
    wp: 0.1
    wg: 0.1
    wz: 0.1
    sc: 0.1
    sg: 0.1
    sz: 0.1
    pm: 0.1
    pg: 0.1
    pz: 0.1
    ev: 0.3
    ex: 0.1
  max_trace_attempts: 3
#- Isolating ---------------------------------
  test_also_iso:
    - HOME
  escape_time:
    default: 6
    0: 6
    1: 6
    2: 6
    3: 6
######################################
##### BEHAVIOUR CHANGE ##############
  omega:
    policy: home
    C: 0.01
    Q: 0.05
    W: 0.05
    D: 0.5
########################
#### INTERVENTIONS #####
  adaptive_lockdown:
  - name: high_lock
    ncumul: 50000
    delay: 0
    level: RED
    exhausted: false
  lockdown_weights:
    RED:
      dw: 1.0
      dg: 1.0
      dz: 1.0
      sc: 0.2
      sg: 0.3
      sz: 0.3
      pm: 0.2
      pg: 0.3
      pz: 0.3
      we: 0.5
      wp: 0.5
      wg: 0.8
      wz: 0.8
      ev: 0.5
      ex: 0.9
  lockdown_testing:
    RED:
      p_test_0: 0.0
      p_test_m: 0.5
      p_test_i:
      - 0.4
      - 0.4
      - 0.4
      - 0.4
      theta_0: 0.33
      theta_m: 0.5
      theta_i: 0.5
      theta_lab: 0.5
  lockdown_shutdown:
    _by_map_:
      OPEN: '00'
      CLOSED: 0X
    RED:
      we: 0.5
      wp: 0.5
      wg: 0.5
      wz: 0.5
      sc: 0.8
      sg: 0.8
      sz: 0.8
      pm: 0.8
      pg: 0.8
      pz: 0.8
      ev: 0.4
      ex: 0.2
  lockdown_state: 0
  group_size_reject:
    default: 1000
    RED: 100
  trace_policy:
    default:
      close: trace
      casual: skip
      EV: skip
      DG: immediate
      DZ: immediate
    RED:
      close: trace
      casual: test
      EV: test
      DG: immediate
      DZ: immediate
