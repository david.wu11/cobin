# Modelling individuals, infections, and interventions
Individuals are modelled as nodes on a bipartite network (in short, a network that can be split into two disjoint groups - in this case interaction contexts and individuals), that are connected to each other through bipartite 'group' nodes corresponding to different interaction contexts (like dwellings, workplaces, schools, etc.). Individuals are able to spread contagion to other individuals if they share a connection through one of these group nodes. The individual nodes also have attributes like the dates that they received vaccine doses and age, that could influence their response to contagion spread (their rate of transmission, risk of developing symptoms, etc.). For more information on how `cobin` uses bipartite networks and for a quick explanation of bipartite networks we have provided a [short explanation](cobin_bipartite) with a more detailed documentation to come.

## Tracking the state of individuals
Individuals in the model are in a given state at any time. These states are represented by two-letter codes, where the first letter represents the disease progression, and the second letter represents the control (TTIQ) measures that the individual is under (for example, self-isolation).

::::{table} Possible disease states
:align: center
| Code     | Description                                                             |
|----------|-------------------------------------------------------------------------|
|     S    |     Susceptible.                                                        |
|     E    |     Exposed. Infected, but not yet infectious. Cannot test positive.    |
|     A    |     Asymptomatic, infected and infectious. Can test positive.           |
|     P    |     Presymptomatic, infected and infectious. Can test positive.         |
|     I    |     Symptomatic, infected and infectious. Can test positive.            |
|     R    |     Recovered. Can test positive.                                       |
|     H    |     Hospitalised.                                                       |
|     C    |     Hospitalised and in critical care.                                  |
|     D    |     Dead.                                                               |

::::

::::{table} Possible disease states
:align: center
| Code | Description                              |
|------|------------------------------------------|
| U    |        Unquarantined                     |
| X    |        Removed (for deceased)            |
| W    | Tested and waiting on result             |
| C    | Confirmed case                           |
| T    | Detected (for recovered confirmed cases) |
| Q    |        Quarantined                       |

::::
In the following documentation, when referring to an individual's state we will use a square/◻ as a wildcard for a letter, if required. The Greek letters shown over a transition arrow indicate the rate at which the jump process occurs for an individual.  Jump processes are defined as processes whereby the state of a 'system' changes by discrete events (e.g. one individual getting sick) where the frequency and nature of these jumps are user-defined reactions. For more information on jump processes, see our documentation of the [simulation algorithm](algorithm.md).


## Disease Progression

````{mermaid}

flowchart LR
    S["S◻"] -->|β| E["E◻"] -->|γ| P["P◻"] & B["B◻"]
    E -->|γ| A["A◻"]
    A["A◻"] ----->|α<sub>IR</sub>| R["R◻"]
    P["P◻"] -->|δ<sub>PI</sub>| I["I◻"] -->|α<sub>IR</sub>| R["R◻"]
    R["R◻"] -->|α<sub>SR</sub>| S["S◻"]
    I["I◻"] -->|δ<sub>IH</sub>| H["H◻"] -->|δ<sub>HC</sub>| C["C◻"] -->|α<sub>CD</sub>| D["D◻"]
    H -->|α<sub>HR</sub>| R
    C -->|α<sub>CR</sub>| R
    I -->|α<sub>ID</sub>| D
    B -->|δ<sub>BM</sub>| M["M◻"] -->|α<sub>MD</sub>| D 
    M ---->|α<sub>MR</sub>| R
````

Individuals that are infected transition into the Exposed (E◻) state, where the virus in is a latent state. In this state, the individual is not infectious, not symptomatic, and will not test positive.

From the Exposed state, an individual can become Asymptomatic (A◻), Presymptomatic (P◻) or Mildly Presymptomatic (M◻) depending on whether they will eventually develop symptoms and the severity of said symptoms. **We note that the (M◻) pathway is currently not used.**

Asymptomatic individuals are infectious, and test positive, but their infection pressure is lower than a symptomatic individual. Asymptomatic individuals will always Recover (R◻) without severe outcomes.

Presymptomatic individuals are infectious, and test positive. They, too, have a lower infection pressure than a symptomatic individual. Presymtomatic individuals progress into the Infected state (I◻), which denotes when they display symptoms, and become more infectious.

Mildly Presymptomatic (B◻) individuals are infectious, and test positive. They, too, have a lower infection pressure than a symptomatic individual (and also lower than a Presymptomatic individual). Mildly Presymptomatic individuals progress into the Mild state (M◻), which denotes when they display (mild) symptoms, and become more infectious.

Infected individuals can recover, but they can also experience more severe outcomes. An Infected individual can become Hospitalised (H◻) if their symptoms get bad enough. Hospitalised individuals can Recover, but if their symptoms worsen, they enter the Critical state (C◻). Critical individuals can either Recover after some time, or Die to complications (D◻). **Although typically not used**, we also model the possibility for Infected individuals to Die without entering the Hospital.

Mild infectious individuals (**although currently not used**) can Recover or Die to complications.


## TTIQ/Control States

```{mermaid}

flowchart TD
    U["◻U"] -->|Waiting on test| W["◻W"] -->|Confirmed positive| C["◻C"] -->|Recovered| T["◻T"]
    W ------>|Negative result or recovery| U
    U["◻U"] -->|Hospitalisation| Q["◻Q"] 
    Q --> |Waiting on test| W["◻W"]
    U["◻U"] -.->|Tracing or test result returned post-recovery| T["◻T"]
    U["◻U"] -->|Testing contact waiting on test| D["◻D"] 
    D -->|Testing contact confirmed| Q["◻Q"]
    D --->|Testing contact recovers| U
```

Individuals are by default in the Unrestricted state (◻U), where their interactions with others are not reduced in any way. Individuals that can test positive (in the Asymptomatic, Presymptomatic or Infected states) can seek tests, and subsequently enter the Waiting (for a test result) state (◻W).

Individuals in the Waiting state may isolate based on government regulations. After some delay (the time needed to analyse a test sample), a Waiting individual will receive their test result. If the result is positive (if they were in a state that would have tested positive when they take their test), they are Confirmed (◻C). They may also recover before the test result is returned and then move back to being Unrestricted. Finally, they may become hospitalised and move into a Quarantined state (◻Q) in the hospital, before receiving a test result.

Confirmed individuals enter a managed self-isolation, and depending on the settings at the time, this may either be in a separate managed facility, or at home. These individuals will self-isolate until they Recover, upon which time they also become Recovered and Tested (RT). They may also develop more severe symptoms and have to visit a hospital in which case they will again move into a Quarantined state.

Household contacts of Waiting individuals may also have to observe at home Distancing (◻D), wherein they also have to isolate at home until the individual waiting for a test receives their test result. They then may have to also have to follow the isolation and quarantine rules as outlined below.

Individuals being confirmed will cause contact tracing procedures to initiate.
Close contacts of Confirmed individuals will be, on a successful contact by contact tracers, be placed into self-isolation or Quarantine (◻Q). If nothing else happens to them (for example, testing positive themselves), they will leave Quarantine and return to being Unrestricted. In the Quarantined state, an individual will have a higher-than-normal rate of test seeking (due to mandated testing), and may subsequently be confirmed.

Casual contacts of Confirmed individuals may also be induced into testing, after some time.
Individuals that have Recovered and are Unrestricted can test positive through this process and become Recovered and Tested.

## Transmission of infection

Infectious individuals (in the Asymptomatic, Presymptomatic and Infected states) can infect others. For more information on the nuances of how we model transmission, see the [description here](transmission_modelling.md).

## Vaccination, Infection-Conferred Protection and Waning Protection

The effects of previously acquired immunity due to vaccination and/or previous infection on disease acquisition, likelihood of symptomatic infection, onwards transmission and severe disease outcomes (hospitalisation, critical care and death) is modelled as an additional rejection probability. Vaccination and previous infections can confer protection, but this protection will wane over time. Protection from death is only included for individuals in critical care not in the case where an individual may die whilst in the infectious state (as this pathway is not normally used).

Individuals who are in the Recovered state are fully protected, but after some time, they return to a Susceptible state. Once in the Susceptible state, their immune protection from vaccines and previous infection wanes as stated above.

The amount of protection is modelled as semi-Markovian (processes whose transition rate does not depend on the transition history but only on the time spent in the current state), where the amount of protection is a function of the type of protection (infection-conferred, vaccine-conferred, or a combination of multiple sources and vaccine doses) and the time since the last conferment of protection (from either receiving a dose of vaccine, or from recovering from infection). The probability of rejection (amount of protection) varies according to a logistic function with respect to time.
This logistic function takes the form

$$\nu(t, t') = \frac{L}{1 + \exp(-k (t-t'))},$$

which is truncated above a threshold value of 1 so that $\nu(t, t') \leq 1$. Here, $t$ is the current time, $t'$ is the time of the previous immunity-conveying event (recovering from infection or vaccination), $L$ is the maximum value of the logistic function, and $k$ is the growth rate.

## Non Pharmaceutical Interventions (NPIs)

Non Pharmaceutical Interventions such as masks are modelled as a groupwise decrease in the infectiousness of individuals in that spreading context. Lockdowns can be set to stop all spread in a group for a given group node attribute (such as the [Traffic Light System in New Zealand](https://covid19.govt.nz/about-our-covid-19-response/history-of-the-covid-19-protection-framework-traffic-lights/) which was a set of NPI policies designed to reduce the spread of COVID-19 in New Zealand) and/or having a set percentage of random groups (for each different group type) 'closed'. More information on how these are modelled can be found [here](simulating_NPIs.md)


## Parameter Configuration
The modelled processes above are controlled by input parameters in a config file to the `networkcontagion`library. Possible input parameters are described [here](input_parameters_dict.md)


## See Also
Contagion Model Methodology [Unpublished Manuscript] (will be linked here once published).
