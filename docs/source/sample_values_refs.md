# References for input and parameter values provided
```{contents}
```
We have provided some example yamls and associated input files that they reference. These serve as examples for the [tutorial](tutorial.md), provide an example of how to set up a config file, as well as describing some sensible input values for disease parameters for 'wild-type' COVID-19 and vaccination parameters for omicron.

This page gives an overview of how we arrived at the values we have provided in the example yamls. More specifically, it is focused on the parameter csv's provided in `/cobin/configs/parameters_by_TA,/cobin/configs/parameters_over_NZ` and the Tutorial/Vignette configuration yamls provided in `/cobin/configs/vignette_*`. The other configuration yamls provided in `/cobin/configs/` are a representative sample of the 'non-implausible' part of the parameter space used by CMA to model COVID-19 during the 2020-2022 period.

We do not describe input values in the example config yamls relating to the simulation set-up and running configuration. For a description of these see [Dictionary of cobin config parameters](input_parameters_dict.md).

## How this page is structured
For the most part it does not make sense to describe the reference for the parameter values one at a time. Instead it makes more sense to describe how we arrived at the values for collection of parameters together. This page is therefore broken up into sections of parameters. These sections align with the sections of code that `configs/vignette_config_0_doses.yaml` is broken into with commented lines. The values referenced in the following explanations are those provided in `configs/vignette_config_0_doses.yaml`, the example config used for the initial [tutorial](tutorial.md). 

## Contagion parameters
The parameter values in this yaml model model wildtype covid-19 in Aotearoa.

### Disease progression
#### Asymptomatic and mild infections
```
    p_asympt_list: !include '/cobin/configs/parameters_by_TA/p_asympt.csv'
    p_mild_list: !include '/cobin/configs/parameters_by_TA/p_mild.csv'
```
For probabilities of asymptomatic infections write up: 
!!!TBD Write up of values determined here. Source: Nick Steyn, analysis of NZ covid data write up here (https://docs.google.com/document/d/1da18BTyqubpxbFDxcwKza73Xhw5O5ymaGoyg2oBw2OI/edit#) 

NB: no 'mild' infections
We have modelled *no* mild infections as taking place, all symptomatic infections are ‘severe’. /cobin/configs/parameters_by_TA/p_mild.csv contains the probability = 0 for all, and therefore any other parameter values in  this yaml that relate to ‘mild’ infections are not used in the model.

#### Incubation period
```python
    gamma:
        scale: 0.9
        shape: 4.1
```
The incubation period is modelled as a random variable with a gamma distribution with shape and scale parameters as shown. These come from !!!TBD gamma distribution parameter refs to come.

#### Symptom progression rates; Recovery and fatality rates; mortality probability;
!!!TBD All labeled in config comments, need to reference where they came from

#### Conditional hospitalisation, critical-condition, fatality probabilities

Taken from Herrara, adjusted for our age bands, made conditional for our model, write up here: (https://docs.google.com/document/d/1da18BTyqubpxbFDxcwKza73Xhw5O5ymaGoyg2oBw2OI/edit#) 


### Transmission related
#### Frequency vs density dependent infection
```python
    density: 1.0
    density_groups: []
    ...
    ...
    beta_h:
        - 0.0
        - 0.0
        - 0.0
        - 0.0
```
These values are set so the simulation runs with frequency-dependent infection. Functionality has been added to `cobin` to allow for density dependent infection within user-specified contexts, but sensible input values have not yet been calibrated. Users are able to modify both the 'density parameter' (to determine how strong the density-dependence is), which groups density-dependence will apply to (we recommend starting with it just in dwellings) and the similar age-structured beta-weighting as in `beta_0` and `beta_c`.

#### Base, relative infectivity
The provided frequency-dependent transmission rate parameters (e.g. `beta_0`, `beta_c`, etc.) are calibrated to give reasonable estimates for the doubling-time distribution, reproductive number and growth rate of the original, 'wildtype' COVID strain for an unvaccinated population on a TA76 network (not provided). This was done by fitting a regression line to log(infections), over a range of assumed generation times.

#### Transmission context weights; event parameters
These provided values are within the plausible region of context weights and the community transmission parameters that were used by CMA during the modelling of the initial Omicron waves circa early-2022.

### Immunity from vaccination and reinfection
We determine the immunity provided from up to four doses of the Pfizer vaccine and from previous infection against COVID-19. Specifically, we focused on the Omicron variant- other assumptions might be more appropriate for earlier or later variants. This immunity provides protection against different stages of disease progression. That is, this immunity makes it less likely to make one of the following state transitions:
* onwards transmission from an infected (A, P, I) individual to a susceptible (S) contact
* acquisition, where an individual moves from susceptible to infected (S to E)
* symptoms, where an individual is symptomatic (E to P)
* hospitalisation, (I to H)
* critical care, (H to C)
* death (C to D).
This immunity wanes over time. We describe the waning immunity with three-parameter logistic functions. These are found using the following steps:
1) Create a dataset of target immunity values for each of the above combinations:
	1.1) We start with Nick Golding's model of waning immunity from vaccination and previous infection due to the role of antibodies. (see https://github.com/goldingn/neuts2efficacy)
	1.2) We compare his model results to real-life data (seee https://doi.org/10.1016/S0140-6736(22)02465-5 for a review). We make slight shifts to the magnitude of one or two immunity curves while keeping the rate of waning the same. 
	1.3) We make an assumption that after 5 months the factor immunity provides to the conditional transition probabilities (that is, the role immunity plays in the * transitions above) to severe illness no longer wane. 
		This makes biological sense since other immune mechanism apart from antibodies play a key role at this stage of the disease and results in a better fit with long-term data. 
	1.4) We assume that the protection from onwards transmission is 0 and in most cases that the extra protection from death given critical care is also 0. 
2) Find the parameters to achieve the target dataset. 
	2.1) First note that the parameters give the conditional transition probabilities. For example, the probability of hospitalisation given symptomatic disease. Thus we fit to logistic conditional probabilities and not to the non-conditional immunity values to get our parameters.
	2.2) In the case of acquisition, the protection against acquisition is measured over a long period of time, however, in our network model there can be many interactions between infectious and susceptible individuals in that time. Thus we need a per-contact value of protection from acquisition. We use model results to calculate the household secondary attack rate (HSAR) for different levels of per-attack protection and underlying infection rate, and use the HSAR to calculate the protection provided over the course of an epidemic wave. We use these model results to establish a numerical relationship between per-contact protection against infection and overall protection against infection in average-sized households. Thus we are able to obtain the per-contact protection required in the model to achieve the target immunity levels. 

### Non-pharmaceutical Interventions and Test Trace Isolate Quarantine
NPI parameters for the one scheduled lockdown were set to limit the total number of infections seen on a TA76 network to a reasonable proportion of the population (during simulations run for a research article). They reduced the total proportion infected from the original 90% to around 40-65%. The TTIQ parameters are based in the plausible region of the parameter space used by CMA during the modelling of the New Zealand COVID-19 Protection Framework by CMA and were then adjusted in a similar way to the NPI parameters.

# Sample networks
We also include two sample networks (in the `example_networks` directory) for usage in running vignettes and as an example of the network objects that are required for the `networkcontagion` library to run on. They are both based on the TA50 [Territorial Authority area](generalising_aotearoa_specific.md), with one having vaccine doses allocated according to . The other is the same network but with all individuals set to have had no vaccine doses.